package com.andrey.math_task;


import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.HapticFeedbackConstants;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {

    EditText mathView;

    //

    ImageView solveBtn, toLeftBtn, toRightBtn, backspaceBtn, radicalBtn, sevenBtn, eightBtn;
    ImageView nineBtn, bracketLBtn, bracketRBtn, squareBtn, fourBtn, fiveBtn, sixBtn, divisionBtn;
    ImageView substractionBtn, fractionBtn, oneBtn, twoBtn, threeBtn, multiBtn, additionBtn;
    ImageView logBtn, zeroBtn, dotBtn, equallyBtn, lessBtn, moreBtn, unknownBtn, moduleBtn;
    ImageView sinBtn, cosBtn, tgBtn, ctgBtn, piBtn, degrBtn, scsBtn, cscBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mathView = (EditText) findViewById(R.id.mathView);

        solveBtn = (ImageView) findViewById(R.id.solveBtn);
        toLeftBtn = (ImageView) findViewById(R.id.toLeftBtn);
        toRightBtn = (ImageView) findViewById(R.id.toRigthBtn);
        backspaceBtn = (ImageView) findViewById(R.id.backspaceBtn);
        radicalBtn = (ImageView) findViewById(R.id.radicalBtn);
        sevenBtn = (ImageView) findViewById(R.id.sevenBtn);
        eightBtn = (ImageView) findViewById(R.id.eightBtn);
        nineBtn = (ImageView) findViewById(R.id.nineBtn);
        bracketLBtn = (ImageView) findViewById(R.id.bracketLBtn);
        bracketRBtn = (ImageView) findViewById(R.id.bracketRBtn);
        squareBtn = (ImageView) findViewById(R.id.squareBtn);
        fourBtn = (ImageView) findViewById(R.id.fourBtn);
        fiveBtn = (ImageView) findViewById(R.id.fiveBtn);
        sixBtn = (ImageView) findViewById(R.id.sixBtn);
        divisionBtn = (ImageView) findViewById(R.id.divisionBtn);
        substractionBtn = (ImageView) findViewById(R.id.substractionBtn);
        fractionBtn = (ImageView) findViewById(R.id.fractionBtn);
        oneBtn = (ImageView) findViewById(R.id.oneBtn);
        twoBtn = (ImageView) findViewById(R.id.twoBtn);
        threeBtn = (ImageView) findViewById(R.id.threeBtn);
        multiBtn = (ImageView) findViewById(R.id.multiBtn);
        additionBtn = (ImageView) findViewById(R.id.additionBtn);
        logBtn = (ImageView) findViewById(R.id.logBtn);
        zeroBtn = (ImageView) findViewById(R.id.zeroBtn);
        dotBtn = (ImageView) findViewById(R.id.dotBtn);
        equallyBtn = (ImageView) findViewById(R.id.equallyBtn);
        lessBtn = (ImageView) findViewById(R.id.lessBtn);
        moreBtn = (ImageView) findViewById(R.id.moreBtn);
        unknownBtn = (ImageView) findViewById(R.id.unknownBtn);
        moduleBtn = (ImageView) findViewById(R.id.moduleBtn);
        sinBtn = (ImageView) findViewById(R.id.sinBtn);
        cosBtn = (ImageView) findViewById(R.id.cosBtn);
        tgBtn = (ImageView) findViewById(R.id.tgBtn);
        ctgBtn = (ImageView) findViewById(R.id.ctgBtn);
        degrBtn = (ImageView) findViewById(R.id.degrBtn);
        piBtn = (ImageView) findViewById(R.id.piBtn);
        scsBtn = (ImageView) findViewById(R.id.scsBtn);
        cscBtn = (ImageView) findViewById(R.id.cscBtn);

        solveBtn.setOnClickListener(this);
        toLeftBtn.setOnClickListener(this);
        toRightBtn.setOnClickListener(this);
        backspaceBtn.setOnClickListener(this);
        radicalBtn.setOnClickListener(this);
        sevenBtn.setOnClickListener(this);
        eightBtn.setOnClickListener(this);
        nineBtn.setOnClickListener(this);
        bracketLBtn.setOnClickListener(this);
        bracketRBtn.setOnClickListener(this);
        squareBtn.setOnClickListener(this);
        fourBtn.setOnClickListener(this);
        fiveBtn.setOnClickListener(this);
        sixBtn.setOnClickListener(this);
        divisionBtn.setOnClickListener(this);
        substractionBtn.setOnClickListener(this);
        fractionBtn.setOnClickListener(this);
        oneBtn.setOnClickListener(this);
        twoBtn.setOnClickListener(this);
        threeBtn.setOnClickListener(this);
        multiBtn.setOnClickListener(this);
        additionBtn.setOnClickListener(this);
        logBtn.setOnClickListener(this);
        zeroBtn.setOnClickListener(this);
        dotBtn.setOnClickListener(this);
        equallyBtn.setOnClickListener(this);
        lessBtn.setOnClickListener(this);
        moreBtn.setOnClickListener(this);
        unknownBtn.setOnClickListener(this);
        moduleBtn.setOnClickListener(this);
        sinBtn.setOnClickListener(this);
        cosBtn.setOnClickListener(this);
        tgBtn.setOnClickListener(this);
        ctgBtn.setOnClickListener(this);
        piBtn.setOnClickListener(this);
        degrBtn.setOnClickListener(this);
        scsBtn.setOnClickListener(this);
        cscBtn.setOnClickListener(this);

        squareBtn.setOnLongClickListener(this);

    }

    @Override
    public void onClick(View v) {

        solveBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        toLeftBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        toRightBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        backspaceBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        radicalBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        sevenBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        eightBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        nineBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        bracketLBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        bracketRBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        squareBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        fourBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        fiveBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        sixBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        divisionBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        substractionBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        fractionBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        oneBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        twoBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        threeBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        multiBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        additionBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        logBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        zeroBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        dotBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        equallyBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        lessBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        moreBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        unknownBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        moduleBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        sinBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        cosBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        tgBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        ctgBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        piBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        degrBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        scsBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        cscBtn.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);

        switch (v.getId()) {

            case R.id.backspaceBtn:

                if (mathView.getText().length() == 0){
                    mathView.setText("0");
                }
                mathView.setText(mathView.getText().delete(mathView.getText().length() - 1, mathView.getText().length()));

                if (mathView.getText().toString().trim().length() == 0) {
                    mathView.setText("0");
                }
                break;

            default:
                if (mathView.getText().toString().equals("0")){
                    mathView.setText(v.getContentDescription().toString());
                } else {
                    mathView.setText(mathView.getText() + v.getContentDescription().toString());
                }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {

            case R.id.squareBtn:
                showPopupMenu(v);
                break;
        }
        return false;
    }

    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.popup);

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu1:
                                mathView.append("³");
                                return true;
                            case R.id.menu2:
                                mathView.append("⁴");
                                return true;
                            case R.id.menu3:
                                mathView.append("⁵");
                                return true;
                            default:
                                return false;
                        }
                    }
                });
        popupMenu.show();
    }
}

